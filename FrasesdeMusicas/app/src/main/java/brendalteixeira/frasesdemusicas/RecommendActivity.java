package brendalteixeira.frasesdemusicas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;


public class RecommendActivity extends AppCompatActivity {

    //UI References
    TextView txtUserName;
    EditText txbSongName, txbArtist, txbQuote, txbTranslation;
    CheckBox checkPtAlready;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend);

        //getting UI references
        txtUserName = (TextView)findViewById(R.id.recommendactivity_txtUserName);
        txbSongName = (EditText)findViewById(R.id.recommendactivity_txbSongName);
        txbArtist = (EditText)findViewById(R.id.recommendactivity_txbArtist);
        txbQuote = (EditText)findViewById(R.id.recommendactivity_txbQuote);
        txbTranslation = (EditText)findViewById(R.id.recommendactivity_txbTranslation);
        checkPtAlready = (CheckBox)findViewById(R.id.recommendactivity_checkPtAlready);

        //handling check box
        checkPtAlready.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                txbTranslation.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            }
        });
    }




    //------------- UI Interactions --------------//
    public void BtnSend_OnClick(View view){

        Intent intent = new Intent(this, SyncActivity.class);
        startActivity(intent);
    }
}


