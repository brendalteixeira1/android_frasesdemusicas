package brendalteixeira.frasesdemusicas.Helpers;


import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import brendalteixeira.frasesdemusicas.Database.Database;
import brendalteixeira.frasesdemusicas.EntryType;
import brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler.AsyncTaskCallbacks;
import brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler.AsyncTask_Result;


public class JsonToDatabase extends AsyncTask<Void, Void, Void>{

    //Attributes
    private Object container = null;
    private final Context CONTEXT;
    private final EntryType ENTRY_TYPE;
    private final String JSON_CONTENT;



    public void SetContainer(Object data) { container = data; }

    public JsonToDatabase(Context context, EntryType entryType, String jsonContent){

        this.CONTEXT = context;
        this.ENTRY_TYPE = entryType;
        this.JSON_CONTENT = jsonContent;
    }



    //----- AsyncTask implementation -------//
    @Override
    protected Void doInBackground(Void... voids) {

        try {

            JSONObject jsonObject = new JSONObject(JSON_CONTENT);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++)
            {
                ContentValues contentValues = new ContentValues();
                jsonObject = jsonArray.getJSONObject(i);
                switch (ENTRY_TYPE)
                {
                    case THEME:
                        contentValues.put(Database.Keys.THEME_ID, jsonObject.getString("id"));
                        contentValues.put(Database.Keys.THEME_NAME, jsonObject.getString("name"));
                        Database.AddEntry(CONTEXT, Database.Keys.THEMES_TABLE, contentValues);
                        break;

                    case ARTIST:
                        contentValues.put(Database.Keys.ARTIST_ID, jsonObject.getString("id"));
                        contentValues.put(Database.Keys.ARTIST_NAME, jsonObject.getString("name"));
                        contentValues.put(Database.Keys.ARTIST_URLPICTURE, jsonObject.getString("urlPicture"));
                        Database.AddEntry(CONTEXT, Database.Keys.ARTISTS_TABLE, contentValues);
                        break;

                    case GENRE:
                        contentValues.put(Database.Keys.GENRE_ID, jsonObject.getString("id"));
                        contentValues.put(Database.Keys.GENRE_NAME, jsonObject.getString("name"));
                        Database.AddEntry(CONTEXT, Database.Keys.GENRE_TABLE, contentValues);
                        break;

                    case QUOTE:
                        contentValues.put(Database.Keys.QUOTE_ID, jsonObject.getString("id"));
                        contentValues.put(Database.Keys.QUOTE_IDARTIST, jsonObject.getString("idArtist"));
                        contentValues.put(Database.Keys.QUOTE_IDGENRE, jsonObject.getString("idGenre"));
                        contentValues.put(Database.Keys.QUOTE_IDRECOMMENDER, jsonObject.getString("idRecommender"));
                        contentValues.put(Database.Keys.QUOTE_IDTHEMES, jsonObject.getString("idTheme"));
                        contentValues.put(Database.Keys.QUOTE_PTBR, jsonObject.getString("ptBR"));
                        contentValues.put(Database.Keys.QUOTE_QUOTE, jsonObject.getString("quote"));
                        contentValues.put(Database.Keys.QUOTE_SONGNAME, jsonObject.getString("songName"));
                        Database.AddEntry(CONTEXT, Database.Keys.QUOTES_TABLE, contentValues);
                        break;

                    case USER:
                        contentValues.put(Database.Keys.USER_ID, jsonObject.getString("id"));
                        contentValues.put(Database.Keys.USER_FACEBOOKID, jsonObject.getString("facebookId"));
                        contentValues.put(Database.Keys.USER_FIRSTNAME, jsonObject.getString("firstName"));
                        contentValues.put(Database.Keys.USER_FULLNAME, jsonObject.getString("fullName"));
                        contentValues.put(Database.Keys.USER_URLPICTURE, jsonObject.getString("urlPicture"));
                        Database.AddEntry(CONTEXT, Database.Keys.USERS_TABLE, contentValues);
                        break;
                }
            }

            //in case of success, callback!
            AsyncTask_Result result = new AsyncTask_Result();
            result.Error = null;
            result.Container = container;
            result.Result = "Conteúdos salvos com sucesso!";
            Callback(result);
        }
        catch (Exception e){

            //in case of success, callback!
            AsyncTask_Result result = new AsyncTask_Result();
            result.Error = new Exception("Erro ao salvar conteúdo. Mensagem: " + e.getMessage());
            result.Container = container;
            result.Result = null;
            Callback(result);
        }

        return null;
    }



    //-------- Handling Callback ------------//
    List<AsyncTaskCallbacks> listeners = new ArrayList<AsyncTaskCallbacks>();

    public void AddListener(AsyncTaskCallbacks listener) { listeners.add(listener); }

    public void RemoveListner(AsyncTaskCallbacks listener) { listeners.remove(listener); }

    public void ClearListeners(){ listeners.clear(); }

    public List<AsyncTaskCallbacks> GetListeners(){ return listeners; }

    void Callback(AsyncTask_Result response) {

        for(AsyncTaskCallbacks listener : listeners)
            if(listener != null)
                listener.OnResult(response);
    }

}
