package brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler;


public class AsyncTask_Result {

    //Attributes
    public Exception Error = null;
    public String Result = "";
    public Object Container = null;


    public Boolean HasSucceeded(){
        return Error == null;
    }
}
