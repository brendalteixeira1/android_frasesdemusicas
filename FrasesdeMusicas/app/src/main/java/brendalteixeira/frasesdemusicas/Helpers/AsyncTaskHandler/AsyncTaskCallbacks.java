package brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler;

import brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler.AsyncTask_Result;


public interface AsyncTaskCallbacks
{
    void OnResult(AsyncTask_Result result);
}
