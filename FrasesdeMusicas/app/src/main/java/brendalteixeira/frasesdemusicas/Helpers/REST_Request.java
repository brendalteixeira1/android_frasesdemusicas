package brendalteixeira.frasesdemusicas.Helpers;


import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler.AsyncTaskCallbacks;
import brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler.AsyncTask_Result;


public class REST_Request extends AsyncTask<String, Void, Void>{

    //Attributes
    private Object container = null;
    private HttpURLConnection connection = null;



    public void SetContainer(Object data) { container = data; }

    public void GET(Map<String, String> headers, String urlTarget){

        try {
            Log.d("tag", "GET | CONNECTING TO --> " + urlTarget.toString());

            //creating requisition
            URL url = new URL(String.format(urlTarget));
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            if(headers != null && !headers.isEmpty())
                for(Map.Entry<String, String> entry : headers.entrySet())
                    connection.addRequestProperty(entry.getKey(), entry.getValue());

            this.execute("GET");
        }
        catch (Exception e)
        {
            //calling back
            AsyncTask_Result result = new AsyncTask_Result();
            result.Container = container;
            result.Error = e;
            Invoke(result);
        }
    }

    public void POST(Map<String, String> headers, Map<String, String> body, String urlTarget){

        try {
            Log.d("tag", "POST | CONNECTING TO --> " + urlTarget.toString());

            //creating requisition
            URL url = new URL(String.format(urlTarget));
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            //setting headers
            if(headers != null && !headers.isEmpty()) {
                Log.d("tag", "POST | HAS HEADERS");
                for (Map.Entry<String, String> entry : headers.entrySet())
                    connection.addRequestProperty(entry.getKey(), entry.getValue());
            }
            //setting body
            StringBuilder postData = null;
            if(body != null && !body.isEmpty()){
                Log.d("tag", "POST | HAS BODY");
                postData = new StringBuilder();
                for (Map.Entry<String, String> param : body.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                }
            }

            this.execute("POST", postData.toString());
        }
        catch (Exception e)
        {
            //calling back
            AsyncTask_Result result = new AsyncTask_Result();
            result.Container = container;
            result.Error = e;
            Invoke(result);
        }
    }



    //----- AsyncTask implementation -------//
    @Override
    protected Void doInBackground(String... params) {

        try {

            //connecting
            switch (params[0]) {

                case "POST":
                    byte[] postDataBytes = params[1].getBytes("UTF-8");
                    connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                    connection.setDoOutput(true); //telling to connection that we are sending output data on body
                    connection.getOutputStream().write(postDataBytes);
                    break;

                case "GET":
                    this.connection.connect();
                    break;
            }

            //parsing the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer json = new StringBuffer(1024);
            String tmp="";
            while((tmp = reader.readLine())!= null)
                json.append(tmp).append("\n");
            reader.close();

            //calling back
            AsyncTask_Result result = new AsyncTask_Result();
            result.Container = container;
            result.Result = json.toString();
            Invoke(result);
        }
        catch (Exception e) {

            //calling back
            AsyncTask_Result result = new AsyncTask_Result();
            result.Container = container;
            result.Error = e;
            Invoke(result);
        }

        return null;
    }



    //-------- Handling Callback ------------//
    List<AsyncTaskCallbacks> listeners = new ArrayList<AsyncTaskCallbacks>();

    public void AddListener(AsyncTaskCallbacks listener) { listeners.add(listener); }

    public void RemoveListner(AsyncTaskCallbacks listener) { listeners.remove(listener); }

    public void ClearListeners(){ listeners.clear(); }

    public List<AsyncTaskCallbacks> GetListeners(){ return listeners; }

    void Invoke(AsyncTask_Result response) {

        for(AsyncTaskCallbacks listener : listeners)
            if(listener != null)
                listener.OnResult(response);
    }
}

