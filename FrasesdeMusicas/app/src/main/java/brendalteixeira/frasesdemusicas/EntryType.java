package brendalteixeira.frasesdemusicas;


public enum EntryType {

    THEME,
    ARTIST,
    GENRE,
    QUOTE,
    USER
}
