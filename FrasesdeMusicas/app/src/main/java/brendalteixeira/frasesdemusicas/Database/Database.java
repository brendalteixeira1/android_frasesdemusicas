package brendalteixeira.frasesdemusicas.Database;

/* --------------------------- DOCUMENTATION -------------------------------------------
   https://developer.android.com/training/basics/data-storage/databases.html#DbHelper
---------------------------------------------------------------------------------------*/

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;


public class Database extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "database.db";
    private static SQLiteDatabase database = null;


    public Database(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        //creating Artists tables...
        Map<String, String> colunmsDefinition = new HashMap<>();
        colunmsDefinition.put(Keys.ARTIST_ID, Keys.TYPE_ID);
        colunmsDefinition.put(Keys.ARTIST_NAME, Keys.TYPE_TEXT);
        colunmsDefinition.put(Keys.ARTIST_URLPICTURE, Keys.TYPE_BIGTEXT);
        CreateTable(Keys.ARTISTS_TABLE, colunmsDefinition, database);

        //creating Genres tables...
        colunmsDefinition = new HashMap<>();
        colunmsDefinition.put(Keys.GENRE_ID, Keys.TYPE_ID);
        colunmsDefinition.put(Keys.GENRE_NAME, Keys.TYPE_TEXT);
        CreateTable(Keys.GENRE_TABLE, colunmsDefinition, database);

        //creating Quotes tables...
        colunmsDefinition = new HashMap<>();
        colunmsDefinition.put(Keys.QUOTE_ID, Keys.TYPE_ID);
        colunmsDefinition.put(Keys.QUOTE_QUOTE, Keys.TYPE_BIGTEXT);
        colunmsDefinition.put(Keys.QUOTE_PTBR, Keys.TYPE_BIGTEXT);
        colunmsDefinition.put(Keys.QUOTE_SONGNAME, Keys.TYPE_TEXT);
        colunmsDefinition.put(Keys.QUOTE_IDTHEMES, Keys.TYPE_TEXT);
        colunmsDefinition.put(Keys.QUOTE_IDARTIST, Keys.TYPE_INT);
        colunmsDefinition.put(Keys.QUOTE_IDGENRE, Keys.TYPE_INT);
        colunmsDefinition.put(Keys.QUOTE_IDRECOMMENDER, Keys.TYPE_INT);
        colunmsDefinition.put(Keys.QUOTE_ALREADYUSED, Keys.TYPE_BOOLEAN);
        CreateTable(Keys.QUOTES_TABLE, colunmsDefinition, database);

        //creating Themes tables...
        colunmsDefinition = new HashMap<>();
        colunmsDefinition.put(Keys.THEME_ID, Keys.TYPE_ID);
        colunmsDefinition.put(Keys.THEME_NAME, Keys.TYPE_TEXT);
        CreateTable(Keys.THEMES_TABLE, colunmsDefinition, database);

        //creating Users tables...
        colunmsDefinition = new HashMap<>();
        colunmsDefinition.put(Keys.USER_ID, Keys.TYPE_ID);
        colunmsDefinition.put(Keys.USER_FACEBOOKID, Keys.TYPE_TEXT);
        colunmsDefinition.put(Keys.USER_FIRSTNAME, Keys.TYPE_TEXT);
        colunmsDefinition.put(Keys.USER_FULLNAME, Keys.TYPE_TEXT);
        colunmsDefinition.put(Keys.USER_URLPICTURE, Keys.TYPE_BIGTEXT);
        CreateTable(Keys.USERS_TABLE, colunmsDefinition, database);

        Database.database = database;
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

        //Droping old tables...
        DeleteTable(Keys.ARTISTS_TABLE, database);
        DeleteTable(Keys.GENRE_TABLE, database);
        DeleteTable(Keys.QUOTES_TABLE, database);
        DeleteTable(Keys.THEMES_TABLE, database);
        DeleteTable(Keys.USERS_TABLE, database);

        //Creating new database
        onCreate(database);
    }



    //--------------- SQL Executions ----------------//
    private void CreateTable(String tableName, Map<String, String> columnsAndTypes, SQLiteDatabase database){

        //creating Query...
        String query = "";
        for (Map.Entry<String, String> entry: columnsAndTypes.entrySet()) {

            query += query.length() == 0 ? ("CREATE TABLE " + tableName + " (") : ", ";
            query += entry.getKey() + " " + entry.getValue();
        }
        query += ")";

        //running Query...
        database.execSQL(query);
    }

    private void DeleteTable(String tableName, SQLiteDatabase database){

        String query = "DROP TABLE IF EXISTS " + tableName;
        database.execSQL(query);
    }

    public static long AddEntry(Context context, String tableName, ContentValues columnValue) {

        if(Database.database == null)
            Database.database = new Database(context).getWritableDatabase();

        try {
            return database.insert(tableName, null, columnValue);
        }
        catch (Exception e)
        {
            Log.d("tag", "SQLITE INSER ERROR: --> " + e.getMessage());
            return 0;
        }
    }

    public static void FilterByTheme(Context context, String idTheme){

        SQLiteDatabase database = new Database(context).getReadableDatabase();
        String query = "SELECT " +
            Keys.QUOTE_ID  + ", " +
            Keys.USER_FULLNAME + ", " +
            Keys.USER_URLPICTURE + ", " +
            Keys.QUOTE_QUOTE + ", " +
            Keys.QUOTE_PTBR + ", " +
            Keys.QUOTE_SONGNAME + ", " +
            Keys.ARTIST_NAME + " " +
            "FROM " + Keys.QUOTES_TABLE + " " +
            "INNER JOIN " + Keys.USERS_TABLE + " ON " + Keys.QUOTE_IDRECOMMENDER + " = " + Keys.USER_FACEBOOKID + " " +
            "INNER JOIN " + Keys.ARTISTS_TABLE + " ON " + Keys.QUOTE_IDARTIST + " = " + Keys.ARTIST_ID + " " +
            "WHERE " + Keys.QUOTE_IDTHEMES + " LIKE '%(" + idTheme + ")%'" +
            "AND " + Keys.QUOTE_ALREADYUSED + " = '0' ORDER BY " + Keys.QUOTE_ID + " DESC";
        Cursor cursor = database.rawQuery(query, null);
    }

    public static void FilterByArtist(Context context, int idArtist){

        SQLiteDatabase database = new Database(context).getReadableDatabase();
        String query = "SELECT " +
                Keys.QUOTE_ID  + ", " +
                Keys.USER_FULLNAME + ", " +
                Keys.USER_URLPICTURE + ", " +
                Keys.QUOTE_QUOTE + ", " +
                Keys.QUOTE_PTBR + ", " +
                Keys.QUOTE_SONGNAME + ", " +
                Keys.ARTIST_NAME + " " +
                "FROM " + Keys.QUOTES_TABLE + " " +
                "INNER JOIN " + Keys.USERS_TABLE + " ON " + Keys.QUOTE_IDRECOMMENDER + " = " + Keys.USER_FACEBOOKID + " " +
                "INNER JOIN " + Keys.ARTISTS_TABLE + " ON " + Keys.QUOTE_IDARTIST + " = " + Keys.ARTIST_ID + " " +
                "WHERE " + Keys.QUOTE_IDARTIST + " = '" + idArtist + "'" +
                "AND " + Keys.QUOTE_ALREADYUSED + " = '0' ORDER BY " + Keys.QUOTE_ID + " DESC";
        Cursor cursor = database.rawQuery(query, null);
    }

    public static void FilterByGenre(Context context, int idGenre){

        SQLiteDatabase database = new Database(context).getReadableDatabase();
        String query = "SELECT " +
                Keys.QUOTE_ID  + ", " +
                Keys.USER_FULLNAME + ", " +
                Keys.USER_URLPICTURE + ", " +
                Keys.QUOTE_QUOTE + ", " +
                Keys.QUOTE_PTBR + ", " +
                Keys.QUOTE_SONGNAME + ", " +
                Keys.ARTIST_NAME + " " +
                "FROM " + Keys.QUOTES_TABLE + " " +
                "INNER JOIN " + Keys.USERS_TABLE + " ON " + Keys.QUOTE_IDRECOMMENDER + " = " + Keys.USER_FACEBOOKID + " " +
                "INNER JOIN " + Keys.ARTISTS_TABLE + " ON " + Keys.QUOTE_IDARTIST + " = " + Keys.ARTIST_ID + " " +
                "WHERE " + Keys.QUOTE_IDGENRE + " = '" + idGenre + "'" +
                "AND " + Keys.QUOTE_ALREADYUSED + " = '0' ORDER BY " + Keys.QUOTE_ID + " DESC";
        Cursor cursor = database.rawQuery(query, null);
    }

    public static void FilterByUser(Context context, int idRecommender){

        SQLiteDatabase database = new Database(context).getReadableDatabase();
        String query = "SELECT " +
                Keys.QUOTE_ID  + ", " +
                Keys.USER_FULLNAME + ", " +
                Keys.USER_URLPICTURE + ", " +
                Keys.QUOTE_QUOTE + ", " +
                Keys.QUOTE_PTBR + ", " +
                Keys.QUOTE_SONGNAME + ", " +
                Keys.ARTIST_NAME + " " +
                "FROM " + Keys.QUOTES_TABLE + " " +
                "INNER JOIN " + Keys.USERS_TABLE + " ON " + Keys.QUOTE_IDRECOMMENDER + " = " + Keys.USER_FACEBOOKID + " " +
                "INNER JOIN " + Keys.ARTISTS_TABLE + " ON " + Keys.QUOTE_IDARTIST + " = " + Keys.ARTIST_ID + " " +
                "WHERE " + Keys.QUOTE_IDRECOMMENDER + " = '" + idRecommender + "'" +
                " ORDER BY " + Keys.QUOTE_ID + " DESC";
        Cursor cursor = database.rawQuery(query, null);
    }

    public static void SetAsAlreadyUsed(Context context, String quoteId){

        SQLiteDatabase database = new Database(context).getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Keys.QUOTE_ALREADYUSED, "1");
        database.update(Keys.QUOTES_TABLE, contentValues, "id=?", new String[]{quoteId});
    }

    public static void GetAll(Context context, String tableName) {

        SQLiteDatabase database = new Database(context).getWritableDatabase();
        String query = "SELECT * FROM " + tableName;
        Cursor cursor = database.rawQuery(query, null);
    }



    //-------------- Tables's Mapping ---------------//
    public static class Keys implements BaseColumns{

        private Keys(){} //to avoid instances

        //TYPES
        public static final String TYPE_ID = "INTEGER PRIMARY KEY";
        public static final String TYPE_INT = "INTEGER";
        public static final String TYPE_TEXT = "VARCHAR(255)";
        public static final String TYPE_BIGTEXT = "TEXT";
        public static final String TYPE_BOOLEAN = "BOOLEAN";

        //TABLE ARTISTS
        public static final String ARTISTS_TABLE = "artists";
        public static final String ARTIST_ID = "artist_id";
        public static final String ARTIST_NAME = "artist_name";
        public static final String ARTIST_URLPICTURE = "artist_urlpicture";

        //TABLE GENRE
        public static final String GENRE_TABLE = "genres";
        public static final String GENRE_ID = "genre_id";
        public static final String GENRE_NAME = "genre_name";

        //TABLE QUOTES
        public static final String QUOTES_TABLE = "quotes";
        public static final String QUOTE_ID = "quote_id";
        public static final String QUOTE_QUOTE = "quote_quote";
        public static final String QUOTE_PTBR = "quote_ptbr";
        public static final String QUOTE_SONGNAME = "quote_songname";
        public static final String QUOTE_IDTHEMES = "quote_idthemes";
        public static final String QUOTE_IDARTIST = "quote_idartist";
        public static final String QUOTE_IDGENRE = "quote_idgenre";
        public static final String QUOTE_IDRECOMMENDER = "quote_idrecommender";
        public static final String QUOTE_ALREADYUSED = "quote_alreadyused";

        //TABLE THEMES
        public static final String THEMES_TABLE = "themes";
        public static final String THEME_ID = "theme_id";
        public static final String THEME_NAME = "theme_name";

        //TABLE USERS
        public static final String USERS_TABLE = "users";
        public static final String USER_ID = "user_id";
        public static final String USER_FACEBOOKID = "user_facebookid";
        public static final String USER_FIRSTNAME = "user_firstname";
        public static final String USER_FULLNAME = "user_fullname";
        public static final String USER_URLPICTURE = "user_urlpicture";
    }
}
