package brendalteixeira.frasesdemusicas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler.AsyncTaskCallbacks;
import brendalteixeira.frasesdemusicas.Helpers.JsonToDatabase;
import brendalteixeira.frasesdemusicas.Helpers.REST_Request;
import brendalteixeira.frasesdemusicas.Helpers.AsyncTaskHandler.AsyncTask_Result;

public class SyncActivity extends AppCompatActivity implements AsyncTaskCallbacks {

    //Tags
    private enum Process {
        LOGIN,
        THEMES_GOTTEN, THEMES_SAVED,
        ARTISTS_GOTTEN, ARTISTS_SAVED,
        GENRES_GOTTEN, GENRES_SAVED,
        USERS_GOTTEN, USERS_SAVED,
        QUOTES_GOTTEN, QUOTES_SAVED
    };

    //Attributes
    LinearLayout popupFacebookConnection;
    LinearLayout popupSynchronization;
    TextView txtLog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        popupFacebookConnection = (LinearLayout)findViewById(R.id.popupFacebookConnection);
        popupSynchronization = (LinearLayout)findViewById(R.id.popSynchronization);
        txtLog = (TextView)findViewById(R.id.txtLog);

        AsyncTask_Result result = new AsyncTask_Result();
        result.Container = Process.LOGIN;
        OnResult(result);

    }



    //-------------- UI Interactions -----------------//
    public void BtnFacebookLogIn_OnClick(View view)
    {

    }

    public void BtnNotNow_OnClick(View view)
    {

    }



    //------------ REST Request Callbacks -----------//
    @Override
    public void OnResult(AsyncTask_Result result) {

        if(!result.HasSucceeded())
        {
            //TODO Display error message
            Log.d("tag", "ERROR --> " + result.Error.getMessage());
            return; //stop execution
        }

        Log.d("tag", "LOG --> " + result.Result);

        Process process = (Process) result.Container;
        REST_Request requisition;
        JsonToDatabase jsonToDatabase;
        switch (process)
        {
            case LOGIN:
                //after Log in download Themes...
                requisition = new REST_Request();
                requisition.AddListener(this);
                requisition.SetContainer(Process.THEMES_GOTTEN);
                requisition.GET(null, Settings.API_URL_BASE + "Themes/All");
                break;

            case THEMES_GOTTEN:
                //after download Themes, save them
                jsonToDatabase = new JsonToDatabase(getApplicationContext(), EntryType.THEME, result.Result);
                jsonToDatabase.AddListener(this);
                jsonToDatabase.SetContainer(Process.THEMES_SAVED);
                jsonToDatabase.execute();
                break;

            case THEMES_SAVED:
                //after save Themes, download Artists...
                requisition = new REST_Request();
                requisition.AddListener(this);
                requisition.SetContainer(Process.ARTISTS_GOTTEN);
                requisition.GET(null, Settings.API_URL_BASE + "Artists/All");
                break;

            case ARTISTS_GOTTEN:
                //after download Artists, save them
                jsonToDatabase = new JsonToDatabase(getApplicationContext(), EntryType.ARTIST, result.Result);
                jsonToDatabase.AddListener(this);
                jsonToDatabase.SetContainer(Process.ARTISTS_SAVED);
                jsonToDatabase.execute();
                break;

            case ARTISTS_SAVED:
                //after save Artists, download Genres...
                requisition = new REST_Request();
                requisition.AddListener(this);
                requisition.SetContainer(Process.GENRES_GOTTEN);
                requisition.GET(null, Settings.API_URL_BASE + "Genres/All");
                break;

            case GENRES_GOTTEN:
                //after download Genres, save them
                jsonToDatabase = new JsonToDatabase(getApplicationContext(), EntryType.GENRE, result.Result);
                jsonToDatabase.AddListener(this);
                jsonToDatabase.SetContainer(Process.GENRES_SAVED);
                jsonToDatabase.execute();
                break;

            case GENRES_SAVED:
                //after save Genres, download Quotes...
                requisition = new REST_Request();
                requisition.AddListener(this);
                requisition.SetContainer(Process.QUOTES_GOTTEN);
                requisition.GET(null, Settings.API_URL_BASE + "Quotes/All");
                break;

            case QUOTES_GOTTEN:
                //after download Quotes, save them
                jsonToDatabase = new JsonToDatabase(getApplicationContext(), EntryType.QUOTE, result.Result);
                jsonToDatabase.AddListener(this);
                jsonToDatabase.SetContainer(Process.QUOTES_SAVED);
                jsonToDatabase.execute();
                break;

            case QUOTES_SAVED:
                //after save Quotes, download Users...
                requisition = new REST_Request();
                requisition.AddListener(this);
                requisition.SetContainer(Process.USERS_GOTTEN);
                requisition.GET(null, Settings.API_URL_BASE + "Users/All");
                break;

            case USERS_GOTTEN:
                //after download Users, save them
                jsonToDatabase = new JsonToDatabase(getApplicationContext(), EntryType.USER, result.Result);
                jsonToDatabase.AddListener(this);
                jsonToDatabase.SetContainer(Process.USERS_SAVED);
                jsonToDatabase.execute();
                break;

            case USERS_SAVED:
                //TODO synchronization completed. Give any feedback to user
                break;
        }
    }
}