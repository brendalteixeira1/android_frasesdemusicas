package brendalteixeira.frasesdemusicas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }



    //-------------- UI Interactions ----------------//
    public void BtnMyQuotes_OnClick(View view){

    }

    public void BtnRecommend_OnClick(View view){

        Intent goToRecommendActivity = new Intent(this, RecommendActivity.class);
        startActivity(goToRecommendActivity);
    }

    public void BtnSuport_OnClick(View view){

    }

    public void BtnLogout_OnClick(View view){

    }
}
